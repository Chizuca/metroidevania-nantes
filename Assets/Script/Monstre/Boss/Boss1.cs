﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Boss1 : MonoBehaviour
{
    public int health;
    //public int damage;
    private bool attaqueBoss, detecteBoss;
    private float currentHitDistance, distanceCast;
    private Vector2 originMonster, directionMonster;
    public float speed, stoppingDistance, counter = 5f;
    private Transform target;
    public LayerMask layerPlayer;
    private Animator anim;
    private bool facingLeft, attak;
    public GameObject bloodEffect;
    
    public Slider healthBar;

    void Start()
    {
        anim = GetComponent<Animator>();
    }

   public void Update() // prend les position ect
    {
        originMonster = transform.position;
        directionMonster = transform.forward;

        RaycastHit2D hits = Physics2D.CircleCast(originMonster, 10, directionMonster, distanceCast, layerPlayer);
        if (hits) 
        {  //circle cast pour detecter le joueur et lui foncer dessus 

            target = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();
            Vector3 direction = (target.position - transform.position).normalized;
            if (-direction.x > 0f && facingLeft)
                Flip();
            else if (-direction.x < 0f && !facingLeft)
                Flip();
            if (Vector2.Distance(transform.position, target.position) > stoppingDistance)
            {
                anim.SetBool("detecteBoss", true);
                transform.position = Vector2.MoveTowards(transform.position, target.position, speed * Time.deltaTime);
            }
            else // delais d'attaque et si raycast attaque le joueur
            {
                if (counter > 0f)
                {
                    counter -= Time.deltaTime;
                }
                else
                {
                    attak = true;
                    counter = 5f;
                }

                if (attak)
                {
                    anim.SetBool("attaqueBoss", true);
                    attak = false; 
                }
                else
                {
                    anim.SetBool("attaqueBoss", false);
                }
            }
        }
        healthBar.value = health;
    }

    private void OnDrawGizmosSelected() //pour voir le raycast en rouge
    {
        Gizmos.color = Color.red;
        Debug.DrawLine(originMonster, originMonster + -Vector2.right * distanceCast);
        Gizmos.DrawWireSphere(originMonster + directionMonster * distanceCast, 6);
    }
    void Flip() // pour faire tourner le perso
  {
      facingLeft = !facingLeft;
     
      Vector3 scale = transform.localScale;
      scale.x *= -1;
      transform.localScale = scale; 
  }
  
  public void TakeDamage(int damage) //systeme de damage 
  {
      Instantiate(bloodEffect, transform.position, Quaternion.identity);
      health -= damage;
  }
}