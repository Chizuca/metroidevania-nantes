﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouvMonstre4 : MonoBehaviour
{
    public float speed;
    public float jump;
    public float detectionRange;
    public float health;
    public int nbrEnfant;

    private Rigidbody2D rb;

    public Transform player;
    public LayerMask groundMask;
    private Vector3 direction;
    
    private bool haveBaby;
    private BoxCollider2D box;

    private void Awake()
    {
        haveBaby = true;
    }

    // Start is called before the first frame update
    void Start()
    { //prend info
        health = 100f;
        rb = transform.GetComponent<Rigidbody2D>();
        box = transform.GetComponent<BoxCollider2D>();
    }

    // Update is called once per frame
    void Update()
    {
        //si player est proche de ennemi
        if(Vector2.Distance(transform.position, player.position) < detectionRange)
        {
            ChasePlayer();
        }

        if(health <= 0f) //meurt
        {
            Die();
        }
    }

    //déplace l'ennemi vers le player
    void ChasePlayer()
    {
        float distance = Vector2.Distance(transform.position, player.position);

        direction = (player.position - transform.position).normalized;

        if (distance > 1f)
        {
            //bondir vers le player
            
            if(Physics2D.Raycast(box.bounds.center, Vector2.down, box.bounds.extents.y + 0.05f, groundMask))
            {
                rb.AddForce(new Vector2(0f, jump), ForceMode2D.Impulse);
            }            
            rb.velocity = new Vector2(Mathf.Sign(direction.x) * speed * 0.2f, rb.velocity.y);
        }
    }

    void Die()
    {
        if (haveBaby)
            for (int i = 0; i < nbrEnfant; i++)
                InstantiateBaby();

        Destroy(gameObject);
    }

    //je pense que le nom de la fonction est assez clair
    void InstantiateBaby()
    {
        Debug.Log("C EST PAS TON SCRIPT");
        GameObject obj = Instantiate(gameObject, transform.position, Quaternion.identity, null);
        obj.transform.localScale = obj.transform.localScale / 2f;
        obj.GetComponent<MouvMonstre4>().haveBaby = false;
    }
}
