﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonstreHealth : MonoBehaviour
{
    public float healthMonster;
    public float healthMonsterMax;
    private Vector3 newForce;
    public Rigidbody2D _Rigidbody2D;

    public GameObject bloodEffect;
    
    Animator anim;
    
    void Start()
    { 
        newForce = new Vector3(5, 0, 0);
        anim = GetComponent<Animator>();         //lance animation
    }

    // Update is called once per frame
    void Update()
    {
        if (healthMonster == 0)
        {
            Destroy(gameObject); //meurt / detruit
        }
    }
    
    public void TakeDamage(int damage)
    {    
        _Rigidbody2D.AddForce(newForce,ForceMode2D.Impulse);
        Instantiate(bloodEffect, transform.position, Quaternion.identity); //effet juicy et dégat en fonction du player
        healthMonster -= damage;
    }

}