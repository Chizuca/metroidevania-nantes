﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Egg : MonoBehaviour
{
    public static int nbrDesactivate;
    public bool isActivate;

    // Start is called before the first frame update
    void Start()
    {
        isActivate = true;
        nbrDesactivate = 0;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "Player" && isActivate && MouvBoss2.chasingPhase)
        {
            isActivate = false;
            ChangeColor();
            nbrDesactivate++;
        }
    }

    void ChangeColor()
    {
        transform.GetComponent<SpriteRenderer>().color = Color.grey;
    }
}