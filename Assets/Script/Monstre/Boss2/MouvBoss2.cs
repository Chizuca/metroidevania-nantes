﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouvBoss2 : MonoBehaviour
{
   public float speed;
    public float detectionRange;
    public float stoppingDistance;
    public float health;

    public bool invulnerable;
    public float knockedCountdown;

    public Egg[] eggs;

    private Rigidbody2D rb;
    public Transform player;

    //IL FAUT QUE TU RAJOUTE UN EMPTY GAMEOBJECT DANS TA SALLE POUR
    //DÉFINIR LA HAUTEUR DU BOSS QUAND IL VOLE
    public Transform heightTransform;


    private Vector3 direction;

    private Animator anim;

    public bool facingLeft;
    private BoxCollider2D box;

    public static bool chasingPhase;

    // Start is called before the first frame update
    void Start()
    {
        health = 100f;
        rb = transform.GetComponent<Rigidbody2D>();
        box = transform.GetComponent<BoxCollider2D>();

        rb.isKinematic = true;
        chasingPhase = true;
        invulnerable = true;

        anim = transform.GetComponent<Animator>();
        anim.enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (!invulnerable)
        {
            if (Input.GetKeyDown(KeyCode.A))
            {
                health -= 10;
            }

            //boss est vulnerable pendant 5 sec
            if (knockedCountdown > 0f)
                knockedCountdown -= Time.deltaTime;
            else
                Wakeup();
        }
        else
        {
            if (Vector2.Distance(transform.position, player.position) < detectionRange)
            {
                if (chasingPhase)
                {
                    ChasePlayer();
                    CheckEggs();
                }
            }
        }

        if (health <= 0f)
            Die();
    }

    void CheckEggs()
    {
        int egg = Egg.nbrDesactivate;
        //si 2, 4 ou 6 oeufs sont désactivés
        if(egg % 2 == 0 && egg > 0)
        {
            Egg.nbrDesactivate -= 2;
            Hurt();
        }
    }

    void Hurt()
    {
        chasingPhase = false;
        anim.enabled = true;
        knockedCountdown = 2f;
        rb.isKinematic = false;
        invulnerable = false;
    }

    void Wakeup()
    {
        anim.enabled = false;
        rb.isKinematic = true;
        invulnerable = true;
        knockedCountdown = 0f;
        chasingPhase = true;

        MoveToTargetPoint(); //reset boss position
    }

    void MoveToTargetPoint()
    {
        transform.Translate(0f, 5f, 0f);
        //rb.AddForce(Vector2.up * 0.7f, ForceMode2D.Impulse);

        Vector3 clampPos = transform.position;
        clampPos.y = Mathf.Clamp(clampPos.y, 0f, heightTransform.position.y);
        transform.position = clampPos;
    }

    void ChasePlayer()
    {
        float distance = Vector2.Distance(transform.position, player.position);

        direction = (player.position - transform.position).normalized;
        if (-direction.x > 0f && facingLeft)
            Flip();
        else if (-direction.x < 0f && !facingLeft)
            Flip();

        if (distance > stoppingDistance)
        {
            MoveToTargetPoint();

            //rb.velocity = new Vector2(Mathf.Sign(direction.x) * speed * 0.2f, rb.velocity.y);
            transform.Translate(Mathf.Sign(direction.x) * speed * Time.deltaTime, 0f, 0f);
        }
        else
        {
            //Shoot Eggs if boss above player?
        }
    }

    void Flip()
    {
        facingLeft = !facingLeft;

        Vector3 scale = transform.localScale;
        scale.x *= -1;
        transform.localScale = scale;
    }

    void Die()
    {
        Destroy(gameObject);
    }
}
