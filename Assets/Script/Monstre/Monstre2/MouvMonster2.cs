﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class MouvMonster2 : MonoBehaviour
{
    public int  speed , jumpForce;
    public bool MoveRight;
    private Bounds _bounds;
    private BoxCollider2D collider;
    public LayerMask layerPlayer, layerPlatform;
    private float currentHitDistance , distanceCast , counter = 2f;
    private Vector2 originMonster , directionMonster;
    public Rigidbody2D _Rigidbody2D;
    public Transform firePoint;
    public GameObject bulletPrefab;

    void Start()
      {
          collider = GetComponent<BoxCollider2D>();
      }
  
      void Update()
      {
          _bounds = collider.bounds;
          originMonster = transform.position;   
          directionMonster = transform.forward; 
          
          RaycastHit2D hits = Physics2D.CircleCast(originMonster, 8, directionMonster, distanceCast, layerPlayer);   //prend info
          RaycastHit2D wallHit = Physics2D.Raycast(originMonster ,-Vector2.right , 2f, layerPlatform );
  
          if (wallHit)
          {
              GetComponent<Rigidbody2D>().AddForce(new Vector2(0, jumpForce));  //si touche murs saute 
          }
          else if (hits)
          {
              if (counter > 0f)
              {
                  counter -= Time.deltaTime; //temps passe 
              }
              else
              {
                  Shoot();
                  counter = 2f;  //tir si le counter est fini
              }
              
          }
          else
          {
              if(MoveRight) //si le monstre se déplace vers la droite
              {
                  transform.Translate(2 * Time.deltaTime * speed, 0, 0);     //il ira vers la gauche
                  transform.localScale = new Vector3(0.37825f, transform.localScale.y, transform.localScale.z);
              }
              else //sinon
              {
                  transform.Translate(-2 * Time.deltaTime * speed, 0, 0); // il ira vers la droite
                  transform.localScale = new Vector3(0.37825f, transform.localScale.y, transform.localScale.z);
              }
          }
      }
      private void OnDrawGizmosSelected()  //pour voir le raycast en rouge
      {
          Gizmos.color = Color.red;
          Debug.DrawLine(originMonster, originMonster + -Vector2.right* distanceCast);
          Gizmos.DrawWireSphere(originMonster + directionMonster * distanceCast, 8);
      }
  
      private void OnTriggerEnter2D(Collider2D collision) //pour faire changer de sens le monstre (pour sa round)
      {
          if(collision.gameObject.CompareTag("Turn"))  //quand collision (avec game object tag Turn)
          {
              if (MoveRight)
                  MoveRight = false;        //il va soit tourné à gauche au à droite en fonction d'un booleen qui s'alterne
              else
                  MoveRight = true;
          }
      }
      void Shoot()
      {
          Instantiate(bulletPrefab, firePoint.position, firePoint.rotation); //tire des balles 
      }
      
  }
