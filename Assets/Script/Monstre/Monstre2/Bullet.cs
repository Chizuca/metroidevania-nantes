﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    public Transform targ;
    public int speed;
    private float counter = 1f;

    private void Start()
    {
        targ = GameObject.FindWithTag("Player").transform; //prend info de position du joueur
    }

    void Update()
    {
        if (counter > 0f)
        {
            counter -= Time.deltaTime;

            if (targ != null) //poursuit le joueur
            {
                transform.position = Vector2.MoveTowards(transform.position, targ.transform.position,
                    speed * Time.deltaTime * 2);
            }
        }

        if (counter <= 0f) //se supprime au bout d'un certain temps
        {
            Destroy(gameObject);
        }


    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.tag == "Player") //si touche joueur se supprime
        {
            Destroy(gameObject);
        }

    }
}
