﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouvMonstre3 : MonoBehaviour
{
    public float speed;
    public float detectionRange;

  
    public float explosionForce;
    public float explosionRadius;

    private Rigidbody2D rb;

    public Transform player;
    public LayerMask playerMask;
    private Vector3 direction;
    
    
    void Start()
    {
        //utile ensuite pour l'explosion
        rb = transform.GetComponent<Rigidbody2D>();
        
    }

    // Update is called once per frame
    void Update()
    {
        //si player est proche de ennemi
        if(Vector2.Distance(transform.position, player.position) < detectionRange)
        {
            ChasePlayer();
        }        
    }

    //déplace l'ennemi vers le player
    void ChasePlayer()
    {
        float distance = Vector2.Distance(transform.position, player.position);

        direction = (player.position - transform.position).normalized;
        if (distance > 1f)
        {
            //déplacer ennemi vers player jusqu'à ce qu'il soit assez proche
            rb.velocity = new Vector2( Mathf.Sign(direction.x ) * speed , rb.velocity.y);
        }
    }
    void Explode()
    {        
        Debug.Log("EXPLOSE!!");
        //obtient tout les colliders adjacents
        Collider2D[] colliders = Physics2D.OverlapCircleAll(transform.position, explosionRadius, playerMask);

        foreach(Collider2D col in colliders)
        {
            Debug.Log(col);
            PlayerHealth player = col.GetComponent<PlayerHealth>();
            if(player != null)
            {
                player.DealDamage(100);
            }
        }
        Destroy(gameObject);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        //dès que l'ennemi touche le player, le processus de l'explosion commence
        if(collision.gameObject.tag == "Player")
        {
            StartCoroutine(StartExplosion());
        }
    }

    IEnumerator StartExplosion()
    {
        //attend 1 sec puis appelle Explode()
        yield return new WaitForSeconds(1f);
        Explode();
    }
}
