﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StatuePlayer : MonoBehaviour
{
    public GameObject swordman;
    public GameObject wolves;
    public GameObject cameraController;

    public void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            if (Input.GetKeyDown(KeyCode.J))
            {
                swordman.SetActive(true);
                swordman.transform.localPosition = wolves.transform.localPosition;
                wolves.SetActive(false);
                cameraController.GetComponent<CameraController>().target = swordman;
            }
        }
    }
}
