﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public GameObject camera;
    public GameObject target;
    
    void LateUpdate () //appeler apres uptadte chaque frame
    {
        camera.transform.position = new Vector3(target.transform.position.x, target.transform.position.y, -2);
    }
}
