﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WolvesProjectileLeft : MonoBehaviour
{
    public float speed;
    public int damage;
    public Transform attackPos;
    public LayerMask whatIsEnnemies;
    public float attackRange;
    
    public void Update()
    {
        transform.Translate(Vector3.left * speed * Time.deltaTime);
        Collider2D[] enemiesToDamage = Physics2D.OverlapCircleAll(attackPos.position, attackRange,whatIsEnnemies);
        for (int i = 0; i < enemiesToDamage.Length; i++)
        {
            enemiesToDamage[i].GetComponent<MonstreHealth>().TakeDamage(damage);
            enemiesToDamage[i].GetComponent<Boss1>().TakeDamage(damage);
        }
    }
    
    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(attackPos.position, attackRange);
    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Monstre"))
        {
            Destroy(gameObject);
        }
    }

    private void OnBecameInvisible()
    {
        Destroy(gameObject);
    }
}