﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShowUi : MonoBehaviour
{
    public GameObject tuto;
    
    // Start is called before the first frame update
    void Start()
    {
        tuto.SetActive(false);
    }

    public void OnTriggerEnter2D(Collider2D player)
    {
        if(player.gameObject.tag == "Player")
            tuto.SetActive(true);
    }

    public void OnTriggerExit2D(Collider2D player)
    {
        if(player.gameObject.tag == "Player")
            tuto.SetActive(false);
    }
}
