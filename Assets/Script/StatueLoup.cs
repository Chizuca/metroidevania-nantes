﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StatueLoup : MonoBehaviour
{
    public GameObject swordman;
    public GameObject wolves;
    public GameObject cameraController;

    public void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            if (Input.GetKeyDown(KeyCode.L))
            {
                wolves.SetActive(true);
                wolves.transform.localPosition = swordman.transform.localPosition;
                swordman.SetActive(false);
                cameraController.GetComponent<CameraController>().target = wolves;
            }
        }
    }
}
