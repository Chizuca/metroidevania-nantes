﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class ChangePlayer : MonoBehaviour
{
    public GameObject swordman;
    public GameObject wolves;
    public GameObject cameraController;
 
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.J))
        {
            swordman.SetActive(true);
            swordman.transform.localPosition = wolves.transform.localPosition;
            wolves.SetActive(false);
            cameraController.GetComponent<CameraController>().target = swordman;
        }
    }

    public void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("StatutLoup"))
        {
            if (Input.GetKeyDown(KeyCode.L))
            {
                wolves.SetActive(true);
                wolves.transform.localPosition = swordman.transform.localPosition;
                swordman.SetActive(false);
                cameraController.GetComponent<CameraController>().target = wolves;
            }
        }
    }
}
