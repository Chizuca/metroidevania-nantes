﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TakeKey : MonoBehaviour
{
    public BoxCollider2D doorCollider1;
    public BoxCollider2D doorCollider2;
    public BoxCollider2D doorCollider3;
    public GameObject doorClose1;
    public GameObject doorClose2;
    public GameObject doorClose3;

    public void OnTriggerEnter2D(Collider2D other)
    {
        doorCollider1.GetComponent<BoxCollider2D>().enabled = true;
        doorCollider2.GetComponent<BoxCollider2D>().enabled = true;
        doorCollider3.GetComponent<BoxCollider2D>().enabled = true;
        doorClose1.SetActive(false);
        doorClose2.SetActive(false);
        doorClose3.SetActive(false);
        gameObject.SetActive(false);
    }
}
