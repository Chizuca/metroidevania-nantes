﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAttaque : MonoBehaviour
{
    public float timeBtwAttack;
    public float startTimeBtwAttack;

    public Transform attackPos;
    public LayerMask whatIsEnnemies;
    public float attackRange;
    public int damage;
    
    void Update()
    {
        if (timeBtwAttack <= 0)
        {
            if (Input.GetKey((KeyCode.A)))
            {
                timeBtwAttack = startTimeBtwAttack; //donc on peut attaquer
                Collider2D[] enemiesToDamage = Physics2D.OverlapCircleAll(attackPos.position, attackRange,whatIsEnnemies);
                for (int i = 0; i < enemiesToDamage.Length; i++)
                {
                    enemiesToDamage[i].GetComponent<MonstreHealth>().TakeDamage(damage);
                    enemiesToDamage[i].GetComponent<Boss1>().TakeDamage(damage);
                }
            }
        }
        else
        {
            timeBtwAttack -= Time.deltaTime;
        }
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(attackPos.position, attackRange);
    }
}
