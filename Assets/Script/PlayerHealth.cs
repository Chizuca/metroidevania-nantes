﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerHealth : MonoBehaviour
{
    public float MaxHealth;
    public static float numberPv;
    public Slider healthbar;
    public GameObject gameover;
    
    public Rigidbody2D _Rigidbody2D;
    Vector3 newForce;
    private Vector3 newForceHurt;
        
    Animator anim;

    public void Start()    
    {
        gameover.SetActive(false);
        MaxHealth = 100;
        numberPv = MaxHealth;
        healthbar.value = CalculateHealth();
        
        newForce = new Vector3(0, 14, 0);
        newForceHurt = new Vector3(100000, 0, 0);

        anim = GetComponent<Animator>();
    }

  /*  void FixedUpdate()
    {
        float move = Input.GetAxis("Horizontal");
        anim.SetFloat("Speed", Mathf.Abs(move)); //abcisse car on s'en fou si c'est négatif ou pas
    }
*/
    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Pique")
        {
            anim.SetBool("Hurt", true);
            DealDamage(10);
            _Rigidbody2D.AddForce(newForce, ForceMode2D.Impulse);
            
        }

        else
        {
            anim.SetBool("Hurt", false);
        }
   
        if (numberPv <= 0)
        {
            Destroy(gameObject);
            gameover.SetActive(true);
            Time.timeScale = 0f;
        }
    }

    public void OnCollisionEnter2D(Collision2D collision)
    {
        
        float move = Input.GetAxis("Horizontal");
        anim.SetFloat("Speed", Mathf.Abs(move)); //abcisse car on s'en fou si c'est négatif ou pas
        if (collision.gameObject.tag == "Monstre")
        {
            anim.SetBool("Hurt", true);
            DealDamage((20));

            if (move < 0)
            {
                //_Rigidbody2D.velocity = Vector3.zero;
                //_Rigidbody2D.AddForce(new Vector2(-10f, 10), ForceMode2D.Impulse);
                Debug.Log(_Rigidbody2D.velocity);
            }

            if (move > 0)
            {
                //_Rigidbody2D.velocity = Vector3.zero;
                //_Rigidbody2D.AddForce(new Vector2(-10f, 10), ForceMode2D.Impulse);
                Debug.Log(_Rigidbody2D.velocity);
            }
        
        }
    }

    float CalculateHealth()
    {
        return numberPv / MaxHealth;
    }
    
    public void DealDamage(float damageValue)
    {
        numberPv -= damageValue;
        healthbar.value = CalculateHealth();
    }
}